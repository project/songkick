(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.songkick = {
    attach: function (context, settings) {
      $('a[data-toggle="tab"]').on('click', function(e) {
          window.localStorage.setItem('activeTab', $(e.target).attr('href'));
      });
      var activeTab = window.localStorage.getItem('activeTab');
      if (activeTab) {
          $('#eventsTab a[href="' + activeTab + '"]').tab('show');
          window.localStorage.removeItem("activeTab");
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
