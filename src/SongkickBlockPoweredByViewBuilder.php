<?php

namespace Drupal\songkick;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Provides a trusted callback for the Songkick Poweredby block.
 *
 */
class SongkickBlockPoweredByViewBuilder implements TrustedCallbackInterface {

    /**
     * {@inheritdoc}
     */
    public static function trustedCallbacks() {
     return ['preRender'];    
   }
 
   /**
    * Get a Upcoming & past events API data.
    */
   public static function preRender($build) {
     $id = $build['#block_content']->id();
  $block = \Drupal\block_content\Entity\BlockContent::load($id);
  $block_type = $block->type[0]->target_id;
  if ($block_type == 'songkick_block') {
  	// Get custom Blocks data.
	  $artis_id = $block->field_artist_id[0]->value;
	  $upcoming_show = $block->field_display_upcoming_shows[0]->value;
	  $past_show = $block->field_display_past_shows[0]->value;
	  $event_details = $block->field_ticket_button[0]->value;
	  $events_data = [
	  	'event_details' => $event_details,
	  	'upcoming_show' => $upcoming_show,
	  	'past_show' => $past_show
	  ];
	  $events_data = [
	      '#theme' => 'songkick_events',
	      '#events_data' => $events_data
	  ];

		$client = \Drupal::httpClient();
		$api_key = \Drupal::config('songkick.settings')->get('songkick_key');
		$url = 'http://api.songkick.com/api/3.0/artists/';

		// Upcoming event data.
  	if ($upcoming_show == '1') {
			$apikey = $url . $artis_id. "/calendar.json" . "?apikey=". $api_key;
			$request = $client->get($apikey);
			$body = $request->getBody()->getContents();
			$AllData = (array)json_decode($body);
			// Last page data
			$per_page = $AllData['resultsPage']->perPage;
			$total_entry = $AllData['resultsPage']->totalEntries;
			$page = (int)($total_entry / $per_page);
			//$mainData = $AllData['resultsPage']->results->event;
 			$temp_var = [];
			for ($x = $page; $x >= 0; $x--){ 
				$final_api_key = $apikey . "&page=" . $x;
				$upcoming_event_request = $client->get($final_api_key);
				$upcoming_event_body = $upcoming_event_request->getBody()->getContents();
				$UpcomingEventAllData = json_decode($upcoming_event_body);
				$mainData = $UpcomingEventAllData->resultsPage->results->event;
				$mainData = is_array($mainData) ? array_reverse($mainData) : NULL;

				//$temp_data[] = $mainData;
				if (!empty($mainData)) {
					foreach ($mainData as $value) {
						$temp_var[] = $value;
					}
				}

			}
			$upcoming_events_data = [
	      '#theme' => 'songkick_events',
	      '#upcoming_event' => $temp_var
	    ];
  	}
  	else{
			$upcoming_events_data = [
	      '#theme' => 'songkick_events',
	      '#upcoming_event' => ''
	    ];	
  	}

    // Past event data.
  	if ($past_show == '1') {
			$apikey = $url . $artis_id. "/gigography.json" . "?apikey=". $api_key;
			$request = $client->get($apikey);
			$body = $request->getBody()->getContents();
			$AllData = (array)json_decode($body);

			// Last page data
			$per_page = $AllData['resultsPage']->perPage;
			$total_entry = $AllData['resultsPage']->totalEntries;
			$page = (int)($total_entry / $per_page);

 			$temp_var = [];
			for ($x = $page; $x >= 0; $x--){ 
				$final_api_key = $apikey . "&page=" . $x;
				$past_event_request = $client->get($final_api_key);
				$past_event_body = $past_event_request->getBody()->getContents();
				$PastEventAllData = json_decode($past_event_body);
				$mainData = array_reverse($PastEventAllData->resultsPage->results->event);

				//$temp_data[] = $mainData;
				foreach ($mainData as $value) {
					$temp_var[] = $value;
				}
			}
			//$final_api_key = $apikey . "&page=" . $page;
			// $past_event_request = $client->get($final_api_key);
			// $past_event_body = $past_event_request->getBody()->getContents();
			// $PastEventAllData = (array)json_decode($past_event_body);
			// $mainData = array_reverse($PastEventAllData['resultsPage']->results->event);
			$past_events_data = [
	      '#theme' => 'songkick_events',
	      '#past_event' => $temp_var
	    ];
  	}
  	else{
			$past_events_data = [
	      '#theme' => 'songkick_events',
	      '#past_event' => ''
	    ];
  	}

  	// Merge Upcoming & past evenst data.
  	$event_data = array_merge($upcoming_events_data,$past_events_data,$events_data);

  	// Return events data.
		return $event_data;
    }
   }
 }