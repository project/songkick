<?php

namespace Drupal\songkick\Form\Config;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class Songkickconfig.
 *
 * @package Drupal\songkick\Form\Config
 */
class Songkickconfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'songkick.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'songkick_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $site_config = $this->config('songkick.settings');

    $form['songkick_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Songkick API key'),
      '#default_value' => $site_config->get('songkick_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Save config values.
    $this->config('songkick.settings')
      ->set('songkick_key', $form_state->getValue('songkick_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
