Songkick
================

The Songkick module enables site builders to render Songkick events in blocks.

## Installation

- composer require drupal/songkick


## Credits
Developed by https://www.drupal.org/u/raghvendrag from https://www.drupal.org/cyber-infrastructure-cisin
Sponsored by https://tambourhinoceros.net, an independent record label and music publisher
Maintained by Tambourhinoceros founder https://drupal.org/u/kristofferrom
